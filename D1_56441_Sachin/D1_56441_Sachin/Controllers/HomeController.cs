﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using D1_56441_Sachin.Models;


namespace D1_56441_Sachin.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
       Sachin21Entities obj = new Sachin21Entities();
        public ActionResult login()
        {
            return View("Login");
        }
        [AllowAnonymous]
         public ActionResult AfterLogin(FormCollection collection)
        {
            string Name = collection["PersonName"];

            string Address=collection["Address"];
            Rationing_Card user =obj.Rationing_Card.Where(u=>u.PersonName == Name).FirstOrDefault();
            if(user != null && user.Address==Address)
            {  if(user.Role == "admin")
                {
                    ViewBag.Name = user.PersonName;
                    return Redirect("/Home/AdminHome");
                }
                else{
                    ViewBag.Name = user.PersonName;
                    return Redirect("/Home/Login");

                }
                    
                        
                        }
            else
            {
                return Redirect("/Home/Login");
            }
            
        }
        public ActionResult AdminHome()
        {
            var list = obj.Rationing_Card.ToList();
            var filterlist = from u in list where u.Role == "applicant" select u;
            return View("AdminView",filterlist.ToList());
        }
        public ActionResult Add()
        {
            return View("AddView");

        }
        public ActionResult AfterAdd( Rationing_Card AddAplicant)
        {
            obj.Rationing_Card.Add(AddAplicant);
            obj.SaveChanges();
            return  Redirect ("/Home/AdminHome");
        }
        




    }
}